
# Bonald et l'origine du langage

## Préambule

L'obscurité qui entoure aujourd'hui le nom de Louis de Bonald est l'un de ces faits qui disent où nous en sommes. Outre son rôle de premier plan dans l'histoire du XIXe siècle, Bonald est un des grands noms de la philosophie. Tout le monde l'a lu, s'en est nourri et de nos jours c'est à peine si les professeurs savent de qui il s'agit. Le souvenir de Bonald ne demeure guère que dans les cercles royalistes, et peut-être parmi les juristes qui doivent avoir entendu parler (j'imagine) de la loi Bonald, cette loi de 1816 qui abolit le divorce.

L'un des aspects les plus intéressants de la pensée de Bonald est sa réflexion sur l'origine du langage, que nous nous proposons de résumer dans les lignes qui suivent. Nous nous appuierons principalement sur le chapitre II des *Recherches philosophiques*, intitulé *De l'origine du langage*. Il nous arrivera de citer également le livre sur le divorce (ou plutôt sur la famille), *Du Divorce considéré au XIXe siècle*, les deux sujets, de la société et du langage, étant étroitement liés, puisque *partout où il naît un homme il y a un père, une mère, un enfant, un langage, le ciel, la terre, Dieu et la société* (*Du divorce*, Discours préliminaire).

## L'état de nature et l'invention humaine du langage

La philosophie du XVIIIe siècle, qui *commence par ôter Dieu de l'univers*, et qui *pour expliquer la société, ne remonte pas plus haut que l'homme*, la philosophie du XVIIIe siècle donc a imaginé que les premiers hommes ne vivaient pas en société, et par conséquent n'avaient pas de langage. Le langage ne pouvait donc être qu'une invention humaine. C'est la thèse que soutiennent ces philosophes, notamment l'abbé Condillac.

Jean-Jacques Rousseau, sur cette question, a une position singulière. Lui qui dans ses livres se sert, comme les autres, de cette hypothèse d'un *état de nature* antérieur à l'état social, il affirme que l'invention du langage est une chose impossible, et il laisse à d'autres de résoudre le problème de l'origine du langage.

>>>
Quant à moi, effrayé des difficultés qui se multiplient, et convaincu de l'impossibilité presque démontrée que les langues aient pu naître et s'établir par des moyens purement humains, je laisse à qui voudra l'entreprendre la discussion de ce difficile problème, lequel a été le plus nécessaire, de la société déjà liée, à l'institution des langues, ou des langues déjà inventées, à l'établissement de la société. (*Discours sur l'origine et les fondements de l'inégalité parmi les hommes*, première partie.)
>>>

Bonald, s'engageant sur les pas de Rousseau, démontre l'impossibilité de l'invention humaine du langage. Plus conséquent que Rousseau, il démontre également l'impossibilité de ce que les philosophes du XVIIIe siècle, Rousseau compris, ont appelé *l'état de nature*, hypothèse contraire *au bon sens et à  l'expérience des choses de la vie, fondement de toute bonne manière de philosopher*.

Si l'homme n'a pas inventé le langage, il l'a reçu. Bonald nous fait apercevoir, dans le simple fait que nous parlons, une *preuve physique* de l'existence d'un être intelligent supérieur à l'homme et à l'univers.

## Les choses usuelles : comment l'homme naît et vit sur la terre

La simple expérience des choses de la vie, sans prouver absolument l'impossibilité de l'invention du langage, témoigne cependant contre la thèse de Condillac. Car jamais on n'a observé l'invention du langage ; on observe, toujours et partout, sa *transmission*.

*Nous transmettons tous les jours à nos enfants la langue que nous avons apprise de nos parents.* Les enfants apprennent à parler en répétant ce qu'ils entendent, et s'ils n'entendent pas parler, ils ne parlent pas. C'est le cas, par exemple, des sourds de naissance. C'est le cas également de ces enfants qui ont été abandonnés dans les bois et qu'on a retrouvés des années plus tard. *L'homme d'aujourd'hui reçoit la parole comme l'être ; il ne parle qu'autant qu'il entend parler, et que le langage qu'il entend parler.*

Soutenir la thèse de l'invention du langage, c'est donc imaginer que *l'homme du commencement* a fait, a pu faire, ce que *l'homme d'aujourd'hui* ne fait jamais dans aucune circonstance. Il est plus naturel de supposer que les premiers hommes parlaient, et qu'ils ont *transmis* la parole au genre humain, comme ils lui ont transmis la vie.

>>>
Des gens occupés de science, sans être savants, qui croient mettre dans les choses l’actif à la place du passif en le mettant dans les mots, peuvent dire la matière *organique* pour la matière *organisée,* et rêver l’homme formé spontanément de l’énergie de la matière et de la fermentation de ses parties, et ce premier point supposé, en déduire, comme une conséquence rigoureuse, l’invention de la société, des lois, des arts et du premier de tous, l’art de parler. Mais, lorsqu’on veut appliquer ces pensées fantastiques aux choses usuelles, telles qu’elles se sont toujours passées et qu’elles se passent encore, et descendre aux moyens par lesquels l’homme naît et vit sur la terre, on ne peut s’empêcher de regarder avec mépris ces folies qui ne sont pas ingénieuses ; et l’on demeure convaincu que l’homme, à son origine, a dû naître homme pour pouvoir transmettre la vie et perpétuer le genre humain, et qu’il a dû naître parlant pour pouvoir transmettre la parole et conserver ainsi la société. (*Recherches philosophiques*, chap. II.)
>>>

L'homme a dû naître parlant : il a reçu le langage comme il a reçu la vie, *soit qu'il ait été créé parlant, soit que la connaissance du langage lui ait été inspirée postérieurement à sa naissance*.

Si nos plus lointains ancêtres n'avaient pas parlé, nous ne parlerions pas non plus.

>>>
L'enfant reçoit de ses parents la raison par la communication de la parole, comme il en a reçu l'être par la communication de la vie. Ses parents ont reçu l'un et l'autre de ceux qui les ont précédés. La progression sensible de la population, partout où des causes accidentelles ou locales ne la contrarient pas, prouve, comme toute progression géométrique, un premier terme générateur. Tout peuple, et le genre humain lui-même, est né d'une famille, puisque encore il pourrait recommencer par une famille, si elle restait seule dans l'univers. (*Du Divorce*, chap II.)
>>>

Toute parole humaine est donc une preuve physique, non seulement de l'existence d'une première famille, mais aussi de l'existence d'un Dieu Créateur dont le premier homme et la première famille ont reçu le langage.

>>>
Dans la parole divine est la raison humaine, comme dans la parole du père est la raison de l'enfant. De là vient qu'en grec, *parole* et *raison* s'expriment par le même mot, *logos*. (*Du Divorce*, chap. II.)
>>>

## L'impossible invention du langage

Ce raisonnement sur les choses de la vie, qui suffit à faire douter de l'invention du langage, n'occupe cependant que la première partie de la discussion. Car on peut démontrer *a priori* l'impossibilité de l'invention du langage, c'est-à-dire indépendamment de toute expérience. On peut montrer, en d'autres termes, que l'invention du langage est une chose impossible en soi.

C'est d'ailleurs ce qu'avait fait Rousseau, en affirmant *la nécessité de la parole pour établir l'usage de la parole*. On peut considérer, ajoute Bonald, *que la parole a été nécessaire pour penser même à l'invention du langage*. Il est donc évident que le langage lui-même ne peut pas être une invention.

>>>
Ainsi, la question tout entière du langage réel ou inventé peut être réduite à la démonstration de l'impossibilité de son invention ; et cette démonstration se trouve dans cette proposition sérieusement méditée : QUE L'HOMME PENSE SA PAROLE AVANT DE PARLER SA PENSÉE, ou autrement, QUE L'HOMME NE PEUT PARLER SA PENSÉE SANS PENSER SA PAROLE.
>>>

On pense dans une langue, dans sa langue maternelle. *En apprenant ma langue maternelle*, écrit Bonald, *j'apprends à penser, c'est-à-dire à attacher des pensées à des mots et des mots aux pensées*. Rousseau avait également remarqué le lien entre pensée et discours, et avant lui Socrate qui, dans le *Théétète*, définit la pensée *un discours que l'âme se tient à elle-même*.

Si la pensée est une parole intérieure, alors le langage est le moyen de toute invention : car inventer, c'est penser.

>>>
Aussi les partisans de l'invention du langage ne manquent pas de dire que les hommes observèrent, réfléchirent, comparèrent, jugèrent, etc. ; car il fallait tout cela pour inventer l'art de parler. Mais, je le demande, de quelle nature, je dirais presque de quelle couleur étaient les observations, les réflexions, les comparaisons, les jugements de ces esprits qui n'avaient encore, en cherchant le langage, aucune expression qui pût leur donner la conscience de leur propre pensées ?
>>>

Le langage est nécessairement antérieur à toute invention ; il ne peut pas avoir été lui-même inventé.

## Le langage, expression de la société

Mais il est également impossible que l'homme ait vécu en société avant de connaître le langage (comme Condillac se plaît à l'imaginer), *car il n’y a pas de société sans lois convenues ou imposées, ni de conventions ou d’injonctions sans parole*.

Aristote fait la même remarque dans une page bien connue de la *Politique*. Parce que l'homme est le seul animal qui ait l'usage de la parole, il est aussi *le seul animal qui ait connaissance du bien et du mal, du juste et de l’injuste, et autres notions semblables, sans lesquelles il n’y a ni famille ni cité*.

La constitution du langage, observe encore Bonald, est aussi la constitution de la société, avec ses personnes, leur rang, leur nombre et leur sexe. Dans les trois personnes du langage, on peut reconnaître les trois personnes qui composent toute société : dans la société domestique, le père, la mère et l'enfant ; dans la société publique, le pouvoir, le ministre et le sujet.

>>>
Ainsi, loin que la société ait pu former le langage, le langage, expression de la société, a dû nécessairement être, dès le commencement, complet ou fini comme la société. Ainsi, les *personnes* et leurs relations se retrouvent partout dans le langage, et en sont aussi l’essence et la constitution. Elles y sont les mêmes en nombre et en ordre que dans la société, et cette vérité paraît à découvert dans l’ordre des pronoms personnels, *je, tu, il,* et dans celui des personnes, première, seconde, troisième, c’est-à-dire la personne *qui* parle, celle *à qui* l’on parle, celle *de qui* l’on parle.
>>>

## Conclusion

La philosophie du XVIIIe siècle, *commençant par ôter Dieu de l'univers*, ne peut pas voir autre chose dans le langage une invention humaine. Cette opinion sur l'origine du langage, non seulement a contre elle la façon dont *les choses usuelles, telles qu’elles se sont toujours passées et qu’elles se passent encore* ; mais elle est aussi contraire au simple bon sens.

Or qu'appelle-t-on aujourd'hui théorie de l'évolution, si ce n'est une version rénovée de la philosophie du XVIIIe siècle ? Cette théorie, de quelque façon qu'on la présente, fait descendre l'homme d'aujourd'hui, l'homme qui parle, d'un animal ou d'un homme qui ne parlait pas. Cette théorie implique nécessairement l'invention humaine du langage : elle est manifestement fausse.

Cette réfutation du matérialisme établit *la noble origine de l'homme et de la société, et la seule origine possible de l'art merveilleux du langage*.
